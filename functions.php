<?php

function gruposancarlos_styles(){
    wp_register_style( 'style', get_template_directory_uri() . '/css/style.css', array(), '1.0' );
	wp_register_style( 'csslider', get_template_directory_uri() . '/css/csslider.css', array(), '1.0' );
    wp_enqueue_style( 'style');
    wp_enqueue_style( 'csslider');
    wp_enqueue_script( 'csslider', get_template_directory_uri() . '/js/scripts.js', array(), '1.0.0', true);
	wp_enqueue_script("jquery");
}


add_action( 'wp_enqueue_scripts', 'gruposancarlos_styles');
add_action('get_header', 'remove_admin_login_header');

//menu
function gruposancarlos_menus(){
    register_nav_menus(array(
        'top-menu' => __('Top Menu', 'gruposancarlos'),
        'main-menu' => __('Main Menu', 'gruposancarlos'),
        'social-menu' => __('Social Menu', 'gruposancarlos')
    ));
}
add_action( 'init', 'gruposancarlos_menus');

/* Disable WordPress Admin Bar for all users but admins. */
function remove_admin_login_header() {
    remove_action('wp_head', '_admin_bar_bump_cb');
}
show_admin_bar(false);
/*--*/
//activar svg
function cc_mime_types($mimes) {
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
  }
  add_filter('upload_mimes', 'cc_mime_types');
  //activar imagen destacada
  add_theme_support( 'post-thumbnails' );