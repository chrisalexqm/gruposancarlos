<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link href="https://fonts.googleapis.com/css?family=Oswald:500|Source+Sans+Pro:400,600" rel="stylesheet">    <?php wp_head() ?>
</head>
<body>
    <header>
        <div class="top-bar container">
            <div class="social-menu-container">
                <?php
                     $args = array(
                        'theme_location' => 'social-menu',
                         'container' => 'nav',
                        'container_class' => 'social-menu'
                    );
                    wp_nav_menu( $args );
                ?>
            </div>
            <div class="top-menu-container">
                <?php
                     $args = array(
                        'theme_location' => 'top-menu',
                         'container' => 'nav',
                        'container_class' => 'top-menu'
                    );
                    wp_nav_menu( $args );
                ?>
                <div class="close-responsive-button">
                   <img src="<?php echo get_template_directory_uri(); ?>/img/close-responsive-icon.svg" alt="Menu Responsive">                  
                </div>
                <div class="menu-responsive-button">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/menu-responsive-icon.svg" alt="Menu Responsive">
                </div>  
            </div>
        </div>
        <div class="logo-bar container">
            <div class="logo-container">
                <a href="<?php echo esc_url( home_url('/') ) ; ?>">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/san_carlos_logo.svg" alt="Logo Grupo San Carlos">
                </a>
            </div>
            <div class="main-menu-container">
                    <?php
                        $args = array(
                            'theme_location' => 'main-menu',
                            'container' => 'nav',
                            'container_class' => 'main-menu'
                        );
                        wp_nav_menu( $args );
                    ?>  
            </div>
        </div>
    </header>