<?php get_header(); ?>

    <?php while(have_posts()): the_post(); ?>
        <div class="body-page container">
            <div class="main">
                <div class="main-content">
                    <div class="page-title-withimg">
                        <div class="page-title-withimg-content">
                            <?php the_title( '<h1>', '</h1>'); ?>
                        </div>
                        <?php the_post_thumbnail(); ?>
                    </div>
                    <main class="page-content">
                        <?php the_content(); ?>
                    </main>
                </div>
            </div>
        </div>    
    <?php endwhile; ?>

<?php get_footer(); ?>